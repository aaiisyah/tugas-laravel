<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(request $request){
        //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:qusetions',
            'isi' => 'required'
        ]);

        $query = DB::table('qusetions')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan');
    }

    public function index(){
        $pertanyaan = DB::table('qusetions')->get();
        // dd($posts);
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id){
        $post = DB::table('qusetions')->where('id',$id)->first();
        // dd($post);
        return view('pertanyaan.show', compact('post'));
    }

    public function edit($id){
        $post = DB::table('qusetions')->where('id',$id)->first();
        // dd($post);
        return view('pertanyaan.edit', compact('post'));
    }

    public function update($id, request $request){
        $request->validate([
            'judul' => 'required|unique:qusetions',
            'isi' => 'required'
        ]);
        
        $query = DB::table('qusetions')->where('id',$id)->update([
            'judul' => $request['judul'],
            'isi' => $request['isi']
        ]);
        
        return redirect('/pertanyaan')->with('success','Berhasil update pertanyaan!');
    }

    public function destroy($id){
        $query = DB::table('qusetions')->where('id',$id)->delete();
        return redirect('/pertanyaan')->with('success','Pertanyaan berhasil dihapus');
    }
}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPertanyaanTepatIdToQusetions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('qusetions', function (Blueprint $table) {
            $table->unsignedBigInteger('pertanyaan_tepat_id');

            $table->foreign('pertanyaan_tepat_id')->references('id')->on('answers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('qusetions', function (Blueprint $table) {
            $table->dropForeign(['pertanyaan_tepat_id']);
            $table->dropColumn(['pertanyaan_tepat_id']);
        });
    }
}

@extends ('..adminlte.master')

@section('content')

<div class="mt-3 ml-3">

<div class="card">
      <div class="card-header">
        <h3 class="card-title">Daftar Pertanyaan</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
          @if(session('success'))
          <div class="alert alert-success">
            {{session('success')}}
          </div>
          @endif
        <a class="btn btn-primary" href="/pertanyaan/create">Buat Pertanyaan Baru</a>
        <table id="example1" class="table table-bordered table-striped" >
          <thead>
          <tr>
            <th style="width:10px">#</th>
            <th>Judul</th>
            <th>Isi</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
            @forelse($pertanyaan as $key => $post)
                <tr>
                <td>{{$key+1}}</td>
                <td>{{$post->judul}}</td>
                <td>{{$post->isi}}</td>
                <td style="display: flex;">
                    <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm">show</a>
                    <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-default btn-sm">edit</a>
                    <form action="/pertanyaan/{{$post->id}}" method="post">
                      @csrf
                      @method('DELETE')
                      <input type="submit" value="delete" class="btn btn-danger btn-sm">
                    </form>
                </td>
                </tr>
            @empty
                <tr>
                <td colspan="4" align="center">Tidak Ada Pertanyaan</td>
                </tr>
            @endforelse
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>

</div>

@endsection
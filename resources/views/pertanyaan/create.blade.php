@extends ('..adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="card card-primary">
    <div class="card-header">
    <h3 class="card-title">Buat Pertanyaan Baru</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan" method="post">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="title">Judul</label>
            <input type="text" class="form-control" id="judul" name="judul" value="{{ old('title','') }}" placeholder="Judul Pertanyaan">
            @error('judul')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Pertanyaan</label>
            <input type="text" class="form-control" id="isi" name="isi"  value="{{ old('isi','') }}" placeholder="Isi Pertanyaan">
            @error('isi')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
</div>
<!-- /.card -->
</div>

@endsection
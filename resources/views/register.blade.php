<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
<h2>Buat Account Baru!</h2>
        <h4>Sign Up Form</h4>
        <form action="/selamat_datang" method="POST">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender" id="male" value="0">Male<br>
        <input type="radio" name="gender" id="female" value="1">Female<br>
        <input type="radio" name="gender" id="other" value="2">Other<br>
        <br>
        <label for="negara">Nationality:</label><br><br>
        <select>
            <option value="0">Indonesian</option>
            <option value="1">Singaporean</option>
            <option value="2">Malaysian</option>
            <option value="3">Australian</option>
        </select>
        <br><br>
        <label for="bahasa">Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa" value="0">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa" value="1">English<br>
        <input type="checkbox" name="bahasa" value="2">Others<br><br>
        <label>Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Submit">

        </form>  
</body>
</html>